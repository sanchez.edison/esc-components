import { ReactElement, ReactNode } from "react";
export declare type ButtonProps = {
    children: ReactNode;
    onClick: () => void;
    className?: string;
    disabled?: boolean;
    style?: Object;
};
export declare const Button: ({ children, onClick, className, ...props }: ButtonProps) => ReactElement;
export default Button;
