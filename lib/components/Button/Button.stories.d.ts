import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';
import { Button } from './Button';
declare const _default: ComponentMeta<({ children, onClick, className, ...props }: import("./Button").ButtonProps) => React.ReactElement<any, string | React.JSXElementConstructor<any>>>;
export default _default;
export declare const Primary: ComponentStory<typeof Button>;
