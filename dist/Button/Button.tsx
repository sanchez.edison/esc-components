import {  ReactElement, ReactNode } from "react"


export type ButtonProps = {
    children: ReactNode,
    onClick: () => void,
    className?: string,
    disabled?: boolean,
    style?: Object,
};

export const Button = ({ children, onClick, className, ...props }: ButtonProps) : ReactElement => {
    return (
        <button
            onClick={onClick}
            style={{
                backgroundColor: '#AF5A5A',
                color: '#fff',
                border: 'none',
                borderRadius: '4px',
                cursor: 'pointer',
                fontSize: '1.2rem',
                fontWeight: 'bold',
                padding: '0.5rem 1rem',
                textTransform: 'uppercase',
                transition: 'all 0.2s ease-in-out',
                ...props.style
            }}
            className={className}
        >
            {children}
        </button>
    )
}

export default Button;